function stringNum(input) {
  if (!(typeof input == "string")) {
    return 0;
  }
  let output = "";
  output = input.trim().replaceAll(",", "");
  if (output.charAt(0) == "$") {
    output = output.slice(1);
  } else if (output.charAt(1) == "$") {
    output = output.substring(0, 1) + output.substring(2);
  }

  if (!isNaN(output)) {
    return output;
  } else {
    return 0;
  }
}

module.exports = stringNum;
