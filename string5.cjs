function arrToStr(input) {
  if (!Array.isArray(input)) {
    return "";
  }
  let output = [];
  for (let index = 0; index < input.length; index++) {
    if (input.length == 0) {
      return " ";
    } else if (typeof input[index] == "string") {
      output.push(input[index].trim());
    }
  }
  return output.join(" ");
}
module.exports = arrToStr;
