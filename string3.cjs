function monthDetail(input) {
  if (!(typeof input == "string")) {
    return "";
}
    const dateMatch = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
    const months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];

    if (input.trim().match(dateMatch)) {
      let output = input.split("/");
      let day = parseInt(output[0]);
      let month = parseInt(output[1]);
      if (day > 0 && day <= 31 && month > 0 && month <= 12) {
        return months[month - 1];
      }
    }
  }

module.exports = monthDetail;
