function titleCase(input) {
  if (!(typeof input == "object")) {
    return {};
  }
  let word = [];

  for (let index in input) {
    if (!(typeof input[index] == "string")) {
      return "";
    } else {
      word.push(input[index].trim());
    }
  }

  let str = word.join(" ");
  let sentences = str.toLowerCase().split(" ");
  for (let index = 0; index < sentences.length; index++) {
    sentences[index] =
      sentences[index][0].toUpperCase() + sentences[index].slice(1);
  }

  return sentences.join(" ");
}

module.exports = titleCase;
