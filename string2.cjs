function ipAdress(input) {
  if (!(typeof input == "string")) {
    return [];
  }
  let numberArray = [];
  const ipv4Adress = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
  if (ipv4Adress.test(input)) {
    let output = input.split(".");
    for (let index = 0; index < output.length; index++) {
      if (isNaN(output[index].trim())) {
        return [];
      } else {
        numberArray.push(parseInt(output[index].trim()));
      }
    }
  }
  return numberArray;
}

module.exports = ipAdress;
